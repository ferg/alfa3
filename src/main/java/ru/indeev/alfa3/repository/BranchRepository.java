package ru.indeev.alfa3.repository;

import org.springframework.data.jpa.repository.*;
import ru.indeev.alfa3.model.*;

public interface BranchRepository extends JpaRepository<Branch, Integer> {
}
