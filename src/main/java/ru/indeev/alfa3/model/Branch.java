package ru.indeev.alfa3.model;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "branches")
public class Branch {

    @Id
    Integer id;

    String title;

    double lon;

    double lat;

    String address;
}
