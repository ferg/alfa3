package ru.indeev.alfa3.error;

import lombok.*;

@Data
@NoArgsConstructor
public class ErrorResponse {

    private String status;

}
