package ru.indeev.alfa3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Alfa3Application {

	public static void main(String[] args) {
		SpringApplication.run(Alfa3Application.class, args);
	}

}
