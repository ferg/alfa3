package ru.indeev.alfa3.controller;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.*;
import ru.indeev.alfa3.error.*;

@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({BranchException.class})
    public ResponseEntity handleBranchException(BranchException exc) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setStatus("branch not found");
        return ResponseEntity.status(404).body(errorResponse);
    }
}
