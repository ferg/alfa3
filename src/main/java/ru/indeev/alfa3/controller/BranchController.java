package ru.indeev.alfa3.controller;

import lombok.extern.log4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import ru.indeev.alfa3.error.*;
import ru.indeev.alfa3.model.*;
import ru.indeev.alfa3.repository.*;

import java.util.*;


@Log4j2
@RestController
@RequestMapping(value = "/branches", produces = MediaType.APPLICATION_JSON_VALUE)
public class BranchController {

    @Autowired
    private BranchRepository branchRepository;

    @GetMapping(value = "/{id}")
    public ResponseEntity<Branch> getBranch(@PathVariable(value = "id") Integer id) throws BranchException {

        return ResponseEntity.ok(branchRepository.findById(id).orElseThrow(() -> new BranchException()));
    }

}
